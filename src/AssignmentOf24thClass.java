import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AssignmentOf24thClass {
    private JPanel root;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JLabel totallabel;

    int total = 0;
    void order(String food, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );

        if(confirmation == 0) {
            String selectvalues[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
            String howmany = (String)JOptionPane.showInputDialog(null,
                    "How many would you like to order?", "How Many",
                    JOptionPane.INFORMATION_MESSAGE,null
                    ,selectvalues, selectvalues[0]
            );

            int many = Integer.valueOf(howmany).intValue();
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + " *" + howmany + "   " + price * many + " yen\n");
            total += price * many;
            totallabel.setText("Total    " + total + " yen");
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
        }
    }

    public AssignmentOf24thClass() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hamburger", 350);
            }
        });

        ImageIcon icon1 = new ImageIcon("src/humburger.jpg");
        button1.setIcon(icon1);

        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sandwich", 300);
            }
        });

        ImageIcon icon2 = new ImageIcon("src/sandwitch.jpg");
        button2.setIcon(icon2);

        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Nugget", 200);
            }
        });

        ImageIcon icon3 = new ImageIcon("src/nugget.jpg");
        button3.setIcon(icon3);

        button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("French fries", 250);
            }
        });

        ImageIcon icon4 = new ImageIcon("src/frenchfries.jpg");
        button4.setIcon(icon4);

        button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Cheeze cake", 280);
            }
        });

        ImageIcon icon5 = new ImageIcon("src/cheezecake.jpg");
        button5.setIcon(icon5);

        button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Pudding", 190);
            }
        });

        ImageIcon icon6 = new ImageIcon("src/pudding.jpg");
        button6.setIcon(icon6);

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " + total + " yen.");
                    total = 0;
                    textPane1.setText("");
                    totallabel.setText("Total    " + total + " yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodGUI");
        frame.setContentPane(new AssignmentOf24thClass().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
